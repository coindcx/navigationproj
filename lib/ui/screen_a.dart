import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:networkflutterapp/main.dart';
import 'package:networkflutterapp/ui/screen_b.dart';

class ScreenPageA extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ScreenPageAState();
  }
}

class ScreenPageAState extends State<ScreenPageA> {
  StreamSubscription<ConnectivityResult> subscription;

  bool alertOn = false;

  @override
  void initState() {
    super.initState();
    this.subscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      if (result == ConnectivityResult.none) {
        NetworkDialog alertDialog = new NetworkDialog();
        alertDialog.showInterNetAlert(context);
        alertOn = true;
      } else {
        if (alertOn) {
          Navigator.pop(context);
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return CommonScreen(
      screenName: "Screen A",
      pre: () {
        SystemChannels.platform.invokeMethod('SystemNavigator.pop');
      },
      next: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => ScreenPageB()),
        );
      },
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    if (subscription != null) {
      subscription.cancel();
    }
  }
}

class CommonScreen extends StatelessWidget {
  final Function next;
  final Function pre;

  const CommonScreen({Key key, this.screenName, this.pre, this.next})
      : super(key: key);

  final String screenName;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(screenName),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Center(
              child: Text(
                screenName,
                style: TextStyle(fontSize: 20),
              ),
            ),
            SizedBox(
              height: 40,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                RaisedButton(
                  color: Colors.deepPurpleAccent,
                  child: Text(
                    "PRE",
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    pre();
                  },
                ),

                RaisedButton(
                  color: Colors.indigo,

                  child: Text("NEXT", style: TextStyle(color: Colors.white)),
                  onPressed: next!=null?() {
                    next();
                  }:null,
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
