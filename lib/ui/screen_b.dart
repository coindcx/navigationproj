import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:networkflutterapp/main.dart';
import 'package:networkflutterapp/ui/screen_a.dart';
import 'package:networkflutterapp/ui/screen_c.dart';

class ScreenPageB extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ScreenPageBState();
  }
}

class ScreenPageBState extends State<ScreenPageB> {
  StreamSubscription<ConnectivityResult> subscription;

  bool alertOn=false;

  @override
  void initState() {
    super.initState();
    this.subscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      if (result == ConnectivityResult.none) {
        NetworkDialog alertDialog = new NetworkDialog();
        alertDialog.showInterNetAlert(context);
        alertOn = true;
      } else {
        if (alertOn) {
          Navigator.pop(context);
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return CommonScreen(
      screenName: "Screen B",
      pre: () {
        Navigator.pop(context);
      },
      next: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => ScreenPageC()),
        );
      },
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    if (subscription != null) {
      subscription.cancel();
    }
  }}