import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:networkflutterapp/main.dart';
import 'package:networkflutterapp/ui/screen_a.dart';
import 'package:networkflutterapp/ui/screen_b.dart';

class ScreenPageC extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ScreenPageCState();
  }
}

class ScreenPageCState extends State<ScreenPageC> {
  StreamSubscription<ConnectivityResult> subscription;

  bool alertOn=false;

  @override
  void initState() {
    super.initState();
    this.subscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      if (result == ConnectivityResult.none) {
        NetworkDialog alertDialog = new NetworkDialog();
        alertDialog.showInterNetAlert(context);
        alertOn = true;
      } else {
        if (alertOn) {
          Navigator.pop(context);
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return CommonScreen(
      screenName: "Screen C",
      pre: () {
       Navigator.pop(context);
      },
      /*next: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => ScreenPageB()),
        );
      },*/
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    if (subscription != null) {
      subscription.cancel();
    }
  }
}
