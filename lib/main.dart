import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:giffy_dialog/giffy_dialog.dart';
import 'package:networkflutterapp/ui/screen_a.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: ScreenPageA(),
    );
  }


}



class NetworkDialog {
  void showInterNetAlert(context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (_) => AssetGiffyDialog(

          /* imagePath: 'assets/men_wearing_jacket.gif',*/
            title: Text(
              'No internet connection',
              style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.w600),
            ),
            description: Text(
              'There is no internet connection. Please turn on your internet connection and press continue.',
              textAlign: TextAlign.center,
              style: TextStyle(),
            ),
            /* entryAnimation: EntryAnimation.RIGHT_LEFT,*/
            buttonOkText: Text("Continue"),
            onOkButtonPressed: () {
              Navigator.pop(context);

            },
            // buttonCancelText: ,
            onCancelButtonPressed: () {
             Navigator.pop(context);
            },
            image: Image.asset(
              'assets/images/no_internet.gif',
              fit: BoxFit.cover,
            )));
  }

}



